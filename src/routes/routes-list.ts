import DemoComponent from 'components/pages';

export const routes = [
  {
    exact: true,
    path: '/',
    component: DemoComponent,
  },
];
