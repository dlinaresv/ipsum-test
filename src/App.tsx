import { appStore, history } from '@shared/store';
import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import { Provider } from 'react-redux';
import './App.css';
import { AppRoutes } from './routes/index';

function App() {
  return (
    <Provider store={appStore}>
      <ConnectedRouter history={history}>
        <AppRoutes />
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
