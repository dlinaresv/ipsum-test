export const colors: { [key: string]: { [key: string]: string } } = {
  Iniciado: { bgColor: '#4EC84C', color: '#FFFFFF' },
  'No iniciado': { bgColor: '#EFB500', color: '#333333' },
  Detenido: { bgColor: '#FB8631', color: '#333333' },
  Finalizado: { bgColor: '#999999', color: '#FFFFFF' },
};
