import { History } from 'history';
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import ProjectsDataReducer from './projects';

export const AppReducers = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    projectState: ProjectsDataReducer,
  });
