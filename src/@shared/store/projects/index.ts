const initialState: ProjectsState = {
  dataProjects: [],
  filterApplied: {},
  visibleFilter: false,
};

function ProjectsDataReducer(
  state: ProjectsState = initialState,
  action: ProjectDataTypes
): ProjectsState {
  switch (action.type) {
    case 'GET_PROJECTS_DATA':
      return {
        ...state,
      };
    case 'SET_VISIBLE_FILTER':
      return {
        ...state,
        visibleFilter: action.payload,
      };
    case 'SET_PROJECTS_DATA':
      return {
        ...state,
        dataProjects: action.payload,
        filterApplied: {},
      };
    case 'FILTER_PROJECTS_DATA':
      return {
        ...state,
        dataProjects: action.payload.data,
        filterApplied: action.payload.filters,
      };
    default:
      return state;
  }
}

export default ProjectsDataReducer;
