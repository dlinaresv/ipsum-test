type ProjectDataTypes =
  | { type: 'GET_PROJECTS_DATA' }
  | { type: 'SET_PROJECTS_DATA'; payload: IPropertyData[] }
  | {
      type: 'FILTER_PROJECTS_DATA';
      payload: { filters: IFiltersProjects; data: IPropertyData[] };
    }
  | { type: 'SET_VISIBLE_FILTER'; payload: boolean };
