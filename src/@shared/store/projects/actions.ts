import data from 'data/sample.json';

export const setVisibleFilter = (visible: boolean) => {
  return {
    type: 'SET_VISIBLE_FILTER',
    payload: visible,
  };
};

export const getProjectData = () => {
  return {
    type: 'SET_PROJECTS_DATA',
    payload: data,
  };
};

export const filterProjectData = (filters: IFiltersProjects) => {
  const dataFilter = data
    .filter((item) => {
      if (filters.statusFilter) {
        return item.status === filters.statusFilter;
      } else {
        return item;
      }
    })
    .sort((a: IPropertyData, b: IPropertyData) => {
      if (filters.nameOrder) {
        if (filters.nameOrder === 'az') {
          return a.name > b.name ? 1 : -1;
        } else if (filters.nameOrder === 'za') {
          return a.name < b.name ? 1 : -1;
        }
        return 0;
      }
      return 0;
    });

  return {
    type: 'FILTER_PROJECTS_DATA',
    payload: { filters, data: dataFilter },
  };
};
