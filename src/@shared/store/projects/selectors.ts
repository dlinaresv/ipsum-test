export function getProjectState(state: ReduxStore) {
  return state.projectState;
}

export function getProjectsData(state: ReduxStore) {
  const { dataProjects, filterApplied } = getProjectState(state);

  return { dataProjects, filterApplied };
}

export function getVisibleFilter(state: ReduxStore) {
  const { visibleFilter } = getProjectState(state);

  return { visibleFilter };
}
