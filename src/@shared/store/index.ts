import { createStore } from 'redux';
import { createBrowserHistory } from 'history';

import { AppReducers } from '@shared/store/reducers';

export const history = createBrowserHistory();

export const appStore = createStore(AppReducers(history));
