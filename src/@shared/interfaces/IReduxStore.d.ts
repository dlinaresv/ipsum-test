interface ReduxStore {
  projectState: ProjectsState;
}

interface ProjectsState {
  dataProjects: IPropertyData[];
  filterApplied: IFiltersProjects;
  visibleFilter: boolean;
}
