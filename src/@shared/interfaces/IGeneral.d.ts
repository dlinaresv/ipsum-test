interface IPropertyData {
  name: string;
  address: string;
  city: string;
  status: string;
  price: string;
  currency: string;
  area: string;
  ppc: string;
  pcr: string;
  percent: string;
  datePublish: string;
  users: number;
  fav: boolean;
}

interface IFiltersProjects {
  statusFilter?: string;
  nameOrder?: string;
}
