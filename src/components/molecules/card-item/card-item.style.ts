import styled from 'styled-components';
import ImageProject from 'assets/image-project.png';

export const CardWrapper = styled.div`
  background: #ffffff;
  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  width: 20.49%;
  min-width: 264.98px;
  padding: 0;
  margin: 5vh 2vh;
`;

export const StatusFavWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const InfoWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 20px;
  align-items: center;
`;

export const InfoDateWrapper = styled.div`
  display: flex;
  margin-top: 10px;
`;

export const GeneralInfoWrapper = styled.div`
  background-image: url(${ImageProject});
  background-position: 70%;
  background-repeat: no-repeat;
  background-size: cover;
  color: #ffffff;
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  height: 229px;
  justify-content: space-between;
`;

export const GeneralInfoWrapperInner = styled.div`
  background: linear-gradient(
    0.25deg,
    #000000 10.96%,
    rgba(0, 0, 0, 0.25) 35.16%
  );
  opacity: 0.75;
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  height: 229px;
  padding: 20px;
  opacity: 0.9;
  background-size: cover;
  justify-content: space-between;
`;
