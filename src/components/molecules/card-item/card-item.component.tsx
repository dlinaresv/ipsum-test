import Calendar from 'assets/calendar.svg';
import Check from 'assets/check.svg';
import Money from 'assets/money.svg';
import PCR from 'assets/pcr.svg';
import PPC from 'assets/ppc.svg';
import Rule from 'assets/rule.svg';
import React from 'react';
import { Fav, User } from '../../../assets/icons/icons';
import LabelStatus from '../../atoms/label-status/label-status.component';
import Label from '../../atoms/label/label.component';
import { GeneralInfoWrapperInner } from './card-item.style';
import {
  CardWrapper,
  GeneralInfoWrapper,
  InfoDateWrapper,
  InfoWrapper,
  StatusFavWrapper,
} from './card-item.style';

interface CardItemProps {
  item: IPropertyData;
}

export default function CardItem({ item }: CardItemProps) {
  return (
    <CardWrapper>
      <GeneralInfoWrapper>
        <GeneralInfoWrapperInner>
          <div>
            <StatusFavWrapper>
              <LabelStatus status={item.status} />
              <Fav stroke="white" fill={item.fav ? 'white' : ''} />
            </StatusFavWrapper>
            <InfoDateWrapper>
              <Label>
                <img src={Calendar} />
                <Label>{item.datePublish}</Label>
              </Label>
              <Label>
                <User /> <Label>{item.users}</Label>
              </Label>
            </InfoDateWrapper>
          </div>
          <div>
            <InfoWrapper style={{ margin: 0 }}>
              <Label
                style={{
                  fontWeight: 'bold',
                  fontSize: '18px',
                  lineHeight: '25px',
                }}
              >
                {item.name}
              </Label>
            </InfoWrapper>
            <InfoWrapper style={{ margin: 0 }}>
              <Label>{`${item.address}, ${item.city}`}</Label>
            </InfoWrapper>
          </div>
        </GeneralInfoWrapperInner>
      </GeneralInfoWrapper>
      <div>
        <InfoWrapper
          style={{ borderBottom: '1px solid #999999', paddingBottom: '10px' }}
        >
          <Label>
            <img src={Money} />
            <Label>{`${item.price} ${item.currency}`}</Label>
          </Label>
          <Label>
            <img src={Rule} />
            <Label>{item.area}</Label>
          </Label>
        </InfoWrapper>
        <InfoWrapper>
          <Label>
            <img src={Check} />
            <Label>{item.percent}</Label>
          </Label>
          <Label>
            <img src={PPC} />
            <Label>{item.ppc}</Label>
          </Label>
          <Label>
            <img src={PCR} />
            <Label>{item.pcr}</Label>
          </Label>
        </InfoWrapper>
      </div>
    </CardWrapper>
  );
}
