import HelpIcon from 'assets/help.svg';
import NotificationIcon from 'assets/notification.svg';
import UserImg from 'assets/user.png';
import React from 'react';
import {
  IconWrapper,
  NotificationSectionWrapper,
} from './notification-section.style';

export default function NotificationSection() {
  return (
    <NotificationSectionWrapper>
      <IconWrapper>
        <img src={NotificationIcon} />
      </IconWrapper>
      <IconWrapper>
        <img src={HelpIcon} />
      </IconWrapper>
      <IconWrapper>
        <img src={UserImg} />
      </IconWrapper>
    </NotificationSectionWrapper>
  );
}
