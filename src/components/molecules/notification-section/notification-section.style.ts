import styled from 'styled-components';

export const NotificationSectionWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

export const IconWrapper = styled.div`
  border-left: 1px solid #214c52;
  border-right: 1px solid #214c52;
  width: 42px;
  justify-content: center;
  display: flex;
`;
