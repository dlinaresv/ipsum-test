import React from 'react';
import ButtonComponent from '../../atoms/button/button.component';
import SearchInput from '../../atoms/search-input/search-input.component';
import { InfoBanner, SearchSectionWrapper } from './search-section.style';
import { Add } from 'assets/icons/icons';

export default function SearchSection() {
  return (
    <SearchSectionWrapper>
      <InfoBanner>
        <SearchInput placeholder="Buscar" />
      </InfoBanner>
      <InfoBanner>
        <ButtonComponent icon={<Add />} text="Crear proyecto" />
      </InfoBanner>
    </SearchSectionWrapper>
  );
}
