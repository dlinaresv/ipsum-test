import styled from 'styled-components';

export const SearchSectionWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: flex-end;
  flex-wrap: wrap;
`;

export const InfoBanner = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 10px;
`;

export const SearchInput = styled.input`
  background: #ffffff;
  border: 0.5px solid #333333;
  box-sizing: border-box;
  box-shadow: inset 2px 2px 6px rgba(0, 0, 0, 0.07);
  border-radius: 5px;
  padding: 9px 15px;
  &::after {
  }
`;
