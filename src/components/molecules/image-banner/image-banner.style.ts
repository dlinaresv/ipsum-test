import styled from 'styled-components';

export const ImageBanner = styled.div`
  display: flex;
  flex-direction: row;
  width: 50%;
`;

export const InfoBanner = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 20px;
  justify-content: space-between;
`;
