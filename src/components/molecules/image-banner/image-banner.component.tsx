import LogoCasa from 'assets/logo-casa.png';
import Money from 'assets/money.svg';
import React from 'react';
import { Project, User } from '../../../assets/icons/icons';
import Label from '../../atoms/label/label.component';
import { ImageBanner, InfoBanner } from './image-banner.style';

export default function ImageBannerComponent() {
  return (
    <ImageBanner>
      <img src={LogoCasa} height="65" />
      <InfoBanner>
        <Label
          style={{ fontWeight: 600, fontSize: '24px', lineHeight: '33px' }}
        >
          Quierocasa
        </Label>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Label>
            <Project />
            <Label>{25}</Label>
          </Label>
          <Label>
            <User fill={'#4ABFCE'} />
            <Label>{71}</Label>
          </Label>
          <Label>
            <img src={Money} />
            <Label>891.308.499.537 MXN</Label>
          </Label>
        </div>
      </InfoBanner>
    </ImageBanner>
  );
}
