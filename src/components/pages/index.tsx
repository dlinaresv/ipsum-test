import React, { useEffect } from 'react';
import DisplayProperties from '../organisms/display-properties/display-properties.component';
import Banner from '../organisms/banner/banner.component';
import Header from '../organisms/header/header.component';
import Footer from '../organisms/footer/footer.component';
import { useDispatch } from 'react-redux';
import { getProjectData } from '@shared/store/projects/actions';

export default function DemoComponent() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProjectData());
  }, []);

  return (
    <>
      <Header />
      <Banner />
      <DisplayProperties />
      <Footer />
    </>
  );
}
