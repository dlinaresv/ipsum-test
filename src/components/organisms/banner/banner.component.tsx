import React from 'react';
import { BannerWrapper } from './banner.styles';
import ImageBannerComponent from '../../molecules/image-banner/image-banner.component';
import SearchSection from '../../molecules/search-section/search-section.component';

export default function Banner() {
  return (
    <BannerWrapper>
      <ImageBannerComponent />
      <SearchSection />
    </BannerWrapper>
  );
}
