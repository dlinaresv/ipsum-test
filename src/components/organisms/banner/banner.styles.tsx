import styled from 'styled-components';

export const BannerWrapper = styled.div`
  display: flex;
  position: relative;
  flex-direction: row;
  flex-wrap: wrap;
  padding: 0px 13.89vh;
  background: #ffffff;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.1);
  min-height: 100px;
  justify-content: space-between;
  align-content: center;
`;
