import React from 'react';
import CardItem from '../../molecules/card-item/card-item.component';
import HeaderFilter from '../header-filter/header-filter.component';
import FilterComponent from '../filter-section/filter-section.component';
import {
  ProjectsWrapper,
  PropertiesWrapper,
} from './display-properties.styles';
import { useSelector } from 'react-redux';
import { getProjectsData } from '@shared/store/projects/selectors';

export default function DisplayProperties() {
  const { dataProjects, filterApplied } = useSelector(getProjectsData);

  return (
    <>
      <FilterComponent />
      <PropertiesWrapper>
        <HeaderFilter />
        <ProjectsWrapper
          style={{
            justifyContent:
              dataProjects.length < 4 ? 'flex-start' : 'space-between',
          }}
        >
          {dataProjects.map((itemData: IPropertyData) => (
            <CardItem item={itemData} />
          ))}
        </ProjectsWrapper>
      </PropertiesWrapper>
    </>
  );
}
