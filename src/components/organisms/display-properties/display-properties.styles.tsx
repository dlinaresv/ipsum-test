import styled from 'styled-components';

export const PropertiesWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  padding: 35px 13.89vh 0;
  background-color: #f2feff;
  min-height: 76.4vh;
`;

export const ProjectsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  background-color: #f2feff;
`;
