import styled from 'styled-components';

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  background: #153236;
  height: 40px;
  padding: 0 13.89vh;
  justify-content: space-between;
  align-content: center;
`;
