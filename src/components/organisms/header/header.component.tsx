import React from 'react';
import { HeaderWrapper } from './header.styles';
import ImageBannerComponent from '../../molecules/image-banner/image-banner.component';
import SearchSection from '../../molecules/search-section/search-section.component';
import IpsumLogo from 'assets/ipsum-logo.png';
import NotificationSection from '../../molecules/notification-section/notification-section.component';

export default function Header() {
  return (
    <HeaderWrapper>
      <img src={IpsumLogo} />
      <NotificationSection />
    </HeaderWrapper>
  );
}
