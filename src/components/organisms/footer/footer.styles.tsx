import styled from 'styled-components';

export const FooterWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  background: #153236;
  height: 20px;
  position: relative;
  width: 100%;
  bottom: 0;
`;

export const MessagesWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 0px 13.89vh 29px;
  background-color: #f2feff;
`;
