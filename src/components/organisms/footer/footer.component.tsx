import React from 'react';
import { FooterWrapper, MessagesWrapper } from './footer.styles';
import { Messages } from 'assets/icons/icons';

export default function Footer() {
  return (
    <>
      <MessagesWrapper>
        <Messages />
      </MessagesWrapper>
      <FooterWrapper />
    </>
  );
}
