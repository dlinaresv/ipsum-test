import {
  filterProjectData,
  setVisibleFilter,
} from '@shared/store/projects/actions';
import {
  getProjectsData,
  getVisibleFilter,
} from '@shared/store/projects/selectors';
import { Clear, CloseRounded, Filter } from 'assets/icons/icons';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ButtonComponent from '../../atoms/button/button.component';
import IconButton from '../../atoms/icon-button/icon-button.component';
import Label from '../../atoms/label/label.component';
import { InfoBanner } from '../../molecules/image-banner/image-banner.style';
import { TitleWrapper } from '../header-filter/header-filter.styles';
import {
  ButtonsWrapper,
  FilterWrapper,
  FormWrapper,
  HeaderTitleWrapper,
  InputWrapper,
  SelectStyled,
} from './filter-section.styles';

export default function FilterComponent() {
  const dispatch = useDispatch();
  const [statusFilter, setStatusFilter] = useState('');
  const [nameOrder, setNameOrder] = useState('');
  const [filterText, setFilterText] = useState<string[]>([]);

  const { filterApplied } = useSelector(getProjectsData);
  const { visibleFilter } = useSelector(getVisibleFilter);

  const resetFilters = () => {
    setStatusFilter('');
    setNameOrder('');
    dispatch(filterProjectData({}));
    dispatch(setVisibleFilter(false));
  };

  useEffect(() => {
    const filters = [];
    if (filterApplied.statusFilter || filterApplied.nameOrder) {
      if (filterApplied.statusFilter) {
        filters.push(filterApplied.statusFilter);
      }
      if (filterApplied.nameOrder) {
        filters.push(
          filterApplied.nameOrder === 'az' ? 'De la A a la Z' : 'De la Z a la A'
        );
      }
    }
    setFilterText(filters);
  }, [filterApplied]);

  const applyFilters = useCallback(() => {
    dispatch(filterProjectData({ statusFilter, nameOrder }));
    dispatch(setVisibleFilter(false));
  }, [dispatch, statusFilter, nameOrder]);

  return (
    <FilterWrapper style={{ display: visibleFilter ? 'flex' : 'none' }}>
      <HeaderTitleWrapper>
        <TitleWrapper>
          <Filter fill={'#333333'} />
          <Label
            style={{
              fontWeight: 600,
              fontSize: '18px',
              lineHeight: '25px',
              color: '#000',
            }}
          >
            Filtros y orden
          </Label>
          {(filterApplied.statusFilter || filterApplied.nameOrder) && (
            <Label
              style={{
                fontWeight: 600,
                fontSize: '18px',
                lineHeight: '25px',
                color: '#999999',
              }}
            >
              ({filterText.join(', ')})
            </Label>
          )}
        </TitleWrapper>
        <div>
          <IconButton
            icon={<CloseRounded />}
            onClick={() => {
              dispatch(setVisibleFilter(false));
            }}
            style={{ backgroundColor: '#4ABFCE' }}
          />
        </div>
      </HeaderTitleWrapper>
      <FormWrapper>
        <InputWrapper>
          <Label style={{ fontWeight: 'bold' }}>Estado: </Label>
          <SelectStyled
            value={statusFilter}
            onChange={(e) => setStatusFilter(e.target.value)}
          >
            <option value="" selected>
              Escoje una opción
            </option>
            <option value="Iniciado">Iniciado</option>
            <option value="No iniciado">No iniciado</option>
            <option value="Detenido">Detenido</option>
            <option value="Finalizado">Finalizado</option>
          </SelectStyled>
        </InputWrapper>
        <InputWrapper>
          <Label style={{ fontWeight: 'bold' }}>Nombre: </Label>
          <SelectStyled
            value={nameOrder}
            onChange={(e) => setNameOrder(e.target.value)}
          >
            <option value="" selected>
              Escoje una opción
            </option>
            <option value="az">De la A a la Z</option>
            <option value="za">De la Z a la A</option>
          </SelectStyled>
        </InputWrapper>
        <InputWrapper>
          <Label style={{ fontWeight: 'bold' }}>Fecha: </Label>
          <SelectStyled>
            <option value="" selected>
              Escoje una opción
            </option>
          </SelectStyled>
        </InputWrapper>
        <InputWrapper>
          <Label style={{ fontWeight: 'bold' }}>Tamaño: </Label>
          <SelectStyled>
            <option value="" selected>
              Escoje una opción
            </option>
          </SelectStyled>
        </InputWrapper>
      </FormWrapper>
      <ButtonsWrapper>
        <InfoBanner>
          <ButtonComponent
            onClick={() => {
              resetFilters();
            }}
            text="Limpiar filtros"
            icon={<Clear fill="#4ABFCE" />}
            fill={false}
          />
        </InfoBanner>
        <InfoBanner>
          <ButtonComponent
            onClick={() => {
              applyFilters();
            }}
            text="Aplicar Filtros"
            icon={<Filter stroke="white" />}
          />
        </InfoBanner>
      </ButtonsWrapper>
    </FilterWrapper>
  );
}
