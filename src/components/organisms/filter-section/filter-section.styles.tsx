import styled from 'styled-components';
import Select from 'react-select';
import Down from 'assets/down.svg';

export const HeaderTitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const SelectStyled = styled.select`
  background: #ffffff;
  border: 1px solid #4abfce;
  box-sizing: border-box;
  border-radius: 5px;
  width: 188px;
  margin-left: 20px;
  padding: 9px 21px;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
  outline: none;
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  background: url(${Down}) no-repeat;
  background-position: right 13px top 50%;
`;

export const FormWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: 35px 0px;
  justify-content: space-between;
`;

export const InputWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;

export const FilterWrapper = styled.div`
  display: flex;
  position: absolute;
  flex-direction: column;
  flex-wrap: wrap;
  padding: 45px 13.89vh 30px;
  background: #ffffff;
  box-shadow: 0px 3px 6px rgb(0 0 0 / 10%);
  min-height: 162px;
  left: 0;
  right: 0;
  z-index: 99;
  top: 141px;

  /* justify-content: space-between;
  align-content: center; */
`;
