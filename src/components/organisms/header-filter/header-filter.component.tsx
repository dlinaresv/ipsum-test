import { GridView, ListView, Project } from 'assets/icons/icons';
import React, { useEffect, useState } from 'react';
import Label from '../../atoms/label/label.component';
import {
  TitleWrapper,
  HeaderFilterWrapper,
  FilterIconsWrapper,
} from './header-filter.styles';
import { Filter, FilterActive } from 'assets/icons/icons';
import IconButton from '../../atoms/icon-button/icon-button.component';
import { setVisibleFilter } from '@shared/store/projects/actions';
import { useDispatch, useSelector } from 'react-redux';
import { getProjectsData } from '@shared/store/projects/selectors';

export default function HeaderFilter() {
  const dispatch = useDispatch();
  const { filterApplied } = useSelector(getProjectsData);
  const [filterText, setFilterText] = useState<string[]>([]);
  useEffect(() => {
    const filters = [];
    if (filterApplied.statusFilter || filterApplied.nameOrder) {
      if (filterApplied.statusFilter) {
        filters.push(filterApplied.statusFilter);
      }
      if (filterApplied.nameOrder) {
        filters.push(
          filterApplied.nameOrder === 'az' ? 'De la A a la Z' : 'De la Z a la A'
        );
      }
    } else {
      filters.push('Todos');
    }
    setFilterText(filters);
  }, [filterApplied]);

  return (
    <HeaderFilterWrapper>
      <TitleWrapper>
        <Project fill={'#333333'} />
        <Label
          style={{
            fontWeight: 600,
            fontSize: '18px',
            lineHeight: '25px',
            color: '#000',
          }}
        >
          Proyectos
        </Label>
        <Label
          style={{
            fontWeight: 600,
            fontSize: '18px',
            lineHeight: '25px',
            color: '#999999',
          }}
        >
          ({filterText.join(', ')})
        </Label>
      </TitleWrapper>
      <FilterIconsWrapper>
        <IconButton
          active={!!filterApplied.nameOrder || !!filterApplied.statusFilter}
          icon={
            filterApplied.nameOrder || filterApplied.statusFilter ? (
              <FilterActive />
            ) : (
              <Filter />
            )
          }
          style={{ marginRight: '20px' }}
          onClick={() => {
            dispatch(setVisibleFilter(true));
          }}
        />
        <IconButton
          active={true}
          icon={<GridView />}
          style={{ marginRight: '1px' }}
          left
        />
        <IconButton icon={<ListView />} right />
      </FilterIconsWrapper>
    </HeaderFilterWrapper>
  );
}
