import styled from 'styled-components';

export const TitleWrapper = styled.div`
  display: flex;
`;

export const HeaderFilterWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const FilterIconsWrapper = styled.div`
  display: flex;
`;
