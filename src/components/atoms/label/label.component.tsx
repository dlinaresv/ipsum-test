import React, { ReactNode } from 'react';
import { CSSProperties } from 'styled-components';
import { LabelStyled } from './label.styles';

interface LabelProps {
  children: ReactNode;
  style?: CSSProperties;
}

export default function Label({ children, style }: LabelProps) {
  return <LabelStyled style={style}>{children}</LabelStyled>;
}
