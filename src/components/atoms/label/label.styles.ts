import styled from 'styled-components';

export const LabelStyled = styled.label`
  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  align-items: center;
  font-size: 14px;
  line-height: 19px;
  margin-left: 5px;
  display: flex;
`;
