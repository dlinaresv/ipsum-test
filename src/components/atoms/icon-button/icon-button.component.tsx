import React, { MouseEventHandler, ReactNode } from 'react';
import { CSSProperties } from 'styled-components';
import { IconButtonWrapper } from './icon-button.styles';

interface IconButtonProps {
  icon: ReactNode;
  right?: boolean;
  left?: boolean;
  active?: boolean;
  style?: CSSProperties;
  onClick?: MouseEventHandler<HTMLButtonElement>;
}

export default function IconButton({
  icon,
  left,
  right,
  active,
  style,
  onClick = () => {},
}: IconButtonProps) {
  return (
    <IconButtonWrapper
      onClick={onClick}
      left={left}
      right={right}
      style={style}
      active={active}
    >
      {icon}
    </IconButtonWrapper>
  );
}
