import styled from 'styled-components';

export const IconButtonWrapper = styled.button<{
  right?: boolean;
  left?: boolean;
  active?: boolean;
}>`
  background: ${(props) => (props.active ? '#4ABFCE' : '#999999')};
  border-radius: 5px;
  border: none;
  height: 36px;
  width: 36px;
  justify-content: center;
  display: flex;
  align-items: center;
  ${(props) =>
    props.left
      ? 'border-top-left-radius: 5px;border-bottom-left-radius: 5px; border-top-right-radius: 0px;border-bottom-right-radius: 0px;'
      : ''}
  ${(props) =>
    props.right
      ? 'border-top-right-radius: 5px;border-bottom-right-radius: 5px; border-top-left-radius: 0px;border-bottom-left-radius: 0px;'
      : ''};
  &:hover {
    background: #4abfce;
  }
`;
