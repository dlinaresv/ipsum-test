import styled from 'styled-components';

export const Input = styled.input`
  height: 36px;
  width: 295px;
  outline: none;
  border: 1px solid #333333;
  border-radius: 5px;
  padding: 15px 9px;
  padding-right: 36px;
  box-sizing: border-box;
  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
`;

export const StyledInput = styled.div`
  svg {
    position: absolute;
    right: 0;
    font-size: 6px;
    padding: 9px 8px;
    fill: #333333;
  }

  background: #ffffff;
  box-sizing: border-box;
  box-shadow: inset 2px 2px 6px rgba(0, 0, 0, 0.07);
  &.inputWithIcon {
    position: relative;
  }
`;
