import { Search } from 'assets/icons/icons';
import React, { useState } from 'react';
import { Input, StyledInput } from './search-input.styles';

interface LabelProps {
  placeholder?: string;
}

export default function SearchInput({ placeholder = '' }: LabelProps) {
  const [text, setText] = useState('');
  const handleChange = (event: any) => {
    setText(event.target.value);
  };

  return (
    <StyledInput className={'inputWithIcon'}>
      <Input
        type="text"
        value={text}
        onChange={handleChange}
        placeholder={placeholder}
        onSubmit={(e) => {
          e.preventDefault();
        }}
      />
      <Search fontSize="20px" />
    </StyledInput>
  );
}
