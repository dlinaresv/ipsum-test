import React, { forwardRef, Ref } from 'react';

interface InputFormProps {
  labelText?: string;
  typeInput: string;
  value: any;
  onChange?: any;
  errors?: any;
  name?: string;
  options?: any[];
}

const InputForm = forwardRef(
  (
    {
      labelText,
      typeInput,
      value,
      options,
      errors = [],
      name = '',
      onChange = () => {},
    }: InputFormProps,
    refInput: Ref<any>
  ) => {
    const getInputType = (inputType: string) => {
      switch (inputType) {
        case 'select':
          return <></>;
        default:
          return <></>;
      }
    };

    return getInputType(typeInput);
  }
);

export default InputForm;
