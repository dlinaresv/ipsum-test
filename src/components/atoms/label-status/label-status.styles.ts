import styled from 'styled-components';

export const StatusWrapper = styled.div`
  align-items: center;
  background: #4ec84c;
  border-radius: 3px;
  font-weight: 600;
  font-size: 11px;
  line-height: 15px;
  text-align: center;
  color: #ffffff;
  padding: 1px 0;
  width: 80px;
  justify-content: center;
  display: flex;
`;
