import { colors } from '@shared/helpers/util';
import React from 'react';
import { StatusWrapper } from './label-status.styles';

interface LabelStatusProps {
  status: string;
}

export default function LabelStatus({ status }: LabelStatusProps) {
  return (
    <StatusWrapper
      style={{
        backgroundColor: colors[status] ? colors[status].bgColor : '',
        color: colors[status] ? colors[status].color : '',
      }}
    >
      {status}
    </StatusWrapper>
  );
}
