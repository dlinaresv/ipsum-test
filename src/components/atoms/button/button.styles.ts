import styled from 'styled-components';

export const ButtonWrapper = styled.button<{ fill: boolean }>`
  background: ${(props) => (props.fill ? '#4abfce' : 'white')};
  border-radius: 5px;
  padding: 8px 30px;
  font-size: 14px;
  line-height: 19px;
  text-align: center;
  color: ${(props) => (props.fill ? '#ffffff' : '#4abfce')};
  align-items: center;
  justify-content: space-between;
  display: flex;
  border: ${(props) => (props.fill ? 'none' : '1px solid #4abfce')};
`;
