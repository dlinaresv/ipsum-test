import { Add } from 'assets/icons/icons';
import React, { MouseEventHandler, ReactNode } from 'react';
import Label from '../label/label.component';
import { ButtonWrapper } from './button.styles';

interface ButtonComponentProps {
  icon?: ReactNode;
  text: string;
  fill?: boolean;
  onClick?: MouseEventHandler<HTMLButtonElement>;
}

export default function ButtonComponent({
  icon,
  text,
  fill = true,
  onClick = () => {},
}: ButtonComponentProps) {
  return (
    <ButtonWrapper fill={fill && true} onClick={onClick}>
      {icon}
      <Label>{text}</Label>
    </ButtonWrapper>
  );
}
